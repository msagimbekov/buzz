CREATE DATABASE  IF NOT EXISTS `reddit` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `reddit`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: reddit
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) DEFAULT NULL,
  `CODE` varchar(200) DEFAULT NULL,
  `ORDER` int(11) DEFAULT NULL,
  `IS_MAIN` varchar(45) DEFAULT 'NO',
  PRIMARY KEY (`CATEGORY_ID`),
  UNIQUE KEY `CATEGORY_ID_UNIQUE` (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment_votes`
--

DROP TABLE IF EXISTS `comment_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_votes` (
  `COMMENT_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `VOTE` varchar(45) NOT NULL,
  `VOTE_DATE` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `COMMENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `POST_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `COMMENT` varchar(2000) DEFAULT NULL,
  `COMMENT_DATE` datetime DEFAULT NULL,
  `AUTHOR_NAME` varchar(200) DEFAULT NULL,
  `AUTHOR_SURNAME` varchar(200) DEFAULT NULL,
  `ARCHIVED` int(11) DEFAULT '0',
  PRIMARY KEY (`COMMENT_ID`),
  UNIQUE KEY `COMMENT_ID_UNIQUE` (`COMMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `post_categories`
--

DROP TABLE IF EXISTS `post_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_categories` (
  `POST_ID` int(11) NOT NULL,
  `CATEGORY_CODE` varchar(200) NOT NULL,
  `POST_CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`POST_CATEGORY_ID`),
  UNIQUE KEY `POST_CATEGORY_ID_UNIQUE` (`POST_CATEGORY_ID`),
  KEY `POST_IDX` (`POST_ID`),
  KEY `CATEGORY_IDX` (`CATEGORY_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `post_details`
--

DROP TABLE IF EXISTS `post_details`;
/*!50001 DROP VIEW IF EXISTS `post_details`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `post_details` (
  `post_id` tinyint NOT NULL,
  `link` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `submit_date` tinyint NOT NULL,
  `homepage` tinyint NOT NULL,
  `photo_url` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `author_name` tinyint NOT NULL,
  `author_surname` tinyint NOT NULL,
  `author_id` tinyint NOT NULL,
  `archived` tinyint NOT NULL,
  `likes` tinyint NOT NULL,
  `dislikes` tinyint NOT NULL,
  `comments` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

CREATE ALGORITHM=UNDEFINED DEFINER=`reddit`@`%` SQL SECURITY DEFINER VIEW `reddit`.`post_details` AS select `post`.`POST_ID` AS `post_id`,`post`.`LINK` AS `link`,`post`.`TITLE` AS `title`,`post`.`SUBMIT_DATE` AS `submit_date`,`post`.`HOMEPAGE` AS `homepage`,`post`.`PHOTO_URL` AS `photo_url`,`post`.`DESCRIPTION` AS `description`,`post`.`AUTHOR_NAME` AS `author_name`,`post`.`AUTHOR_SURNAME` AS `author_surname`,`post`.`AUTHOR_ID` AS `author_id`,`post`.`ARCHIVED` AS `archived`,(select count(0) from `reddit`.`post_votes` `vote` where ((`vote`.`POST_ID` = `post`.`POST_ID`) and (`vote`.`VOTE` = 'LIKE') and (`vote`.`ARCHIVED` = 0))) AS `likes`,(select count(0) from `reddit`.`post_votes` `vote` where ((`vote`.`POST_ID` = `post`.`POST_ID`) and (`vote`.`VOTE` = 'DISLIKE') and (`vote`.`ARCHIVED` = 0))) AS `dislikes`,(select count(0) from `reddit`.`comments` `comment` where ((`comment`.`POST_ID` = `post`.`POST_ID`) and (`comment`.`ARCHIVED` = 0))) AS `comments` from `reddit`.`posts` `post`;


--
-- Table structure for table `post_tags`
--

DROP TABLE IF EXISTS `post_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tags` (
  `post_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`post_tag_id`),
  UNIQUE KEY `post_tag_id_UNIQUE` (`post_tag_id`),
  KEY `post_idx` (`post_id`),
  FULLTEXT KEY `tag_idx` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `post_votes`
--

DROP TABLE IF EXISTS `post_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_votes` (
  `POST_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `VOTE` varchar(45) NOT NULL,
  `VOTE_DATE` datetime DEFAULT NULL,
  `POST_VOTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ARCHIVED` int(11) DEFAULT '0',
  PRIMARY KEY (`POST_VOTE_ID`),
  UNIQUE KEY `POST_VOTE_ID_UNIQUE` (`POST_VOTE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `POST_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LINK` varchar(2000) DEFAULT NULL,
  `TITLE` varchar(2000) DEFAULT NULL,
  `SUBMIT_DATE` datetime DEFAULT NULL,
  `HOMEPAGE` varchar(200) DEFAULT NULL,
  `PHOTO_URL` varchar(2000) DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `AUTHOR_NAME` varchar(200) DEFAULT NULL,
  `AUTHOR_SURNAME` varchar(200) DEFAULT NULL,
  `AUTHOR_ID` int(11) DEFAULT NULL,
  `ARCHIVED` int(11) DEFAULT '0',
  PRIMARY KEY (`POST_ID`),
  UNIQUE KEY `POST_ID_UNIQUE` (`POST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE` varchar(45) NOT NULL,
  `NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `ROLE_ID_UNIQUE` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `USER_ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ROLE_ID`),
  UNIQUE KEY `USER_ROLE_ID_UNIQUE` (`USER_ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(200) DEFAULT NULL,
  `REG_DATE` date DEFAULT NULL,
  `NAME` varchar(200) DEFAULT NULL,
  `SURNAME` varchar(200) DEFAULT NULL,
  `PASS_HASH` varchar(2000) DEFAULT NULL,
  `SOCIAL_NETWORK` varchar(200) DEFAULT NULL,
  `UID` varchar(200) DEFAULT NULL,
  `USERNAME` varchar(200) DEFAULT NULL,
  `PROFILE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_ID_UNIQUE` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `post_details`
--

/*!50001 DROP TABLE IF EXISTS `post_details`*/;
/*!50001 DROP VIEW IF EXISTS `post_details`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`reddit`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `post_details` AS select `post`.`POST_ID` AS `post_id`,`post`.`LINK` AS `link`,`post`.`TITLE` AS `title`,`post`.`SUBMIT_DATE` AS `submit_date`,`post`.`HOMEPAGE` AS `homepage`,`post`.`PHOTO_URL` AS `photo_url`,`post`.`DESCRIPTION` AS `description`,`post`.`AUTHOR_NAME` AS `author_name`,`post`.`AUTHOR_SURNAME` AS `author_surname`,`post`.`AUTHOR_ID` AS `author_id`,`post`.`ARCHIVED` AS `archived`,(select count(0) from `post_votes` `vote` where ((`vote`.`POST_ID` = `post`.`POST_ID`) and (`vote`.`VOTE` = 'LIKE') and (`vote`.`ARCHIVED` = 0))) AS `likes`,(select count(0) from `post_votes` `vote` where ((`vote`.`POST_ID` = `post`.`POST_ID`) and (`vote`.`VOTE` = 'DISLIKE') and (`vote`.`ARCHIVED` = 0))) AS `dislikes`,(select count(0) from `comments` `comment` where ((`comment`.`POST_ID` = `post`.`POST_ID`) and (`comment`.`ARCHIVED` = 0))) AS `comments` from `posts` `post` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-08 13:10:17
