/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.transaction.Transactional;
import kz.zophr.entity.Post;
import org.apache.commons.io.IOUtils;
import org.apache.commons.validator.UrlValidator;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.stereotype.Service;

/**
 *
 * @author Madi
 */
@Service
public class LinkService extends MainService {

    private void doTrustToCertificates() throws Exception {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    return;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    return;
                }
            }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                    System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                }
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    private String getText(String url, int test) throws Exception {

        if (url.startsWith("https")) {
            doTrustToCertificates();
            System.setProperty("https.protocols", "TLSv1.2");
            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustStrategy() {

                        @Override
                        public boolean isTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
                            return true;
                        }
                    })
                    .useTLS()
                    .build();

            SSLConnectionSocketFactory connectionFactory
                    = new SSLConnectionSocketFactory(sslContext, new AllowAllHostnameVerifier());

            CloseableHttpClient httpclient = HttpClients.custom()
                    .setSSLSocketFactory(connectionFactory)
                    .build();

            HttpEntity httpEntity = httpclient.execute(new HttpGet(url)).getEntity();
            InputStream is = httpEntity.getContent();
            InputStreamReader isr = new InputStreamReader(is);

            StringBuilder buffer;
            try (BufferedReader in = new BufferedReader(isr)) {

                buffer = new StringBuilder();
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    buffer.append(inputLine);
                }
            }
            
            return buffer.toString();
        }

        return Request.Get(url).execute().returnContent().asString();
        /*Response response = Request.Get(url).execute();
         HttpResponse httpResponse = response.returnResponse();
         //Content content = response.returnContent();
         for (Header h : httpResponse.getAllHeaders()) {
         System.out.println(h.getName() + " -- " + h.getValue());
         for (HeaderElement e : h.getElements()) {
         System.out.println(e.getName() + " ** " + e.getValue());
         }
         }
         HttpEntity httpEntity = httpResponse.getEntity();
         InputStream is = httpEntity.getContent();
         Header header = httpEntity.getContentType();
        
         System.out.println("*************");
         System.out.println(header.toString());
         for (HeaderElement elem : header.getElements()) {
         System.out.println(elem.getValue() + " ---");
         }
         System.out.println("*************");
         String charset = header.getValue();

         InputStreamReader isr;
         if (charset != null) {
         isr = new InputStreamReader(is, charset);
         } else {
         isr = new InputStreamReader(is);
         }

         StringBuilder buffer;
         try (BufferedReader in = new BufferedReader(isr)) {

         buffer = new StringBuilder();
         String inputLine;
         while ((inputLine = in.readLine()) != null) {
         buffer.append(inputLine);
         }
         }*/

        /*CloseableHttpClient httpclient = HttpClients.createDefault();
         try {
         HttpGet httpget = new HttpGet(url);

         System.out.println("Executing request " + httpget.getRequestLine());

         // Create a custom response handler
         ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

         @Override
         public String handleResponse(
         final HttpResponse response) throws ClientProtocolException, IOException {
         int status = response.getStatusLine().getStatusCode();
         if (status >= 200 && status < 300) {
         HttpEntity entity = response.getEntity();
         return entity != null ? EntityUtils.toString(entity) : null;
         } else {
         throw new ClientProtocolException("Unexpected response status: " + status);
         }
         }

         };
         String responseBody = httpclient.execute(httpget, responseHandler);
         System.out.println("----------------------------------------");
         System.out.println(responseBody);
         return responseBody;
         } finally {
         httpclient.close();
         }*/
        //return buffer.toString();
        //return content.asString(Charset.forName(charset));
    }

    private String getText(String url) throws Exception {
        doTrustToCertificates();
        System.setProperty("https.protocols", "TLSv1.2");
        URL u = new URL(url);

        URLConnection conn;
        if (url.startsWith("https")) {
            conn = (HttpsURLConnection) u.openConnection();
        } else {
            conn = (HttpURLConnection) u.openConnection();
        }
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
        conn.setRequestProperty("Accept-Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        //StringBuilder buffer;
        //String charset;

        //ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //byte[] buf = new byte[1024];
        //int n = 0;
        String enc = "UTF-8";
        /*try {
         String contentType = conn.getContentType();
         if (contentType != null) {
         int k = contentType.indexOf("charset=");
         if (k != -1) {
         int last = contentType.indexOf(";", k);
         if (last != -1) {
         enc = contentType.substring(k + "charset=".length(), last);
         } else {
         enc = contentType.substring(k + "charset=".length());
         }
         }
         }
         } catch (Exception e) {

         }*/
        return IOUtils.toString(conn.getInputStream(), enc);
        //return IOUtils.toString(conn.getInputStream()); 

        /*while ((n = conn.getInputStream().read(buf)) >= 0) {
         baos.write(buf, 0, n);
         }
         byte[] content = baos.toByteArray();

         try (InputStream is = new ByteArrayInputStream(content)) {
         byte[] buff1 = new byte[4096];
         UniversalDetector detector = new UniversalDetector(null);
         int nread;
         while ((nread = is.read(buff1)) > 0 && !detector.isDone()) {
         detector.handleData(buff1, 0, nread);
         }
         detector.dataEnd();
         charset = detector.getDetectedCharset();
         }

         InputStreamReader isr;
         if (charset != null) {
         isr = new InputStreamReader(new ByteArrayInputStream(content), charset);
         } else {
         isr = new InputStreamReader(new ByteArrayInputStream(content));
         }

         try (BufferedReader in = new BufferedReader(isr)) {

         buffer = new StringBuilder();
         String inputLine;
         while ((inputLine = in.readLine()) != null) {
         buffer.append(inputLine);
         }
         }
         return buffer.toString();*/
    }

    public int getResponseCode(String urlString, String protocol) throws MalformedURLException, IOException {
        if (!urlString.startsWith("http://") && !urlString.startsWith("https://")) {
            String prefix = "//";
            if (urlString.startsWith("//")) {
                prefix = "";
            }
            urlString = protocol + ":" + prefix + urlString;
        }
        URL u = new URL(urlString);
        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
        huc.setRequestMethod("GET");
        huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
        huc.connect();
        return huc.getResponseCode();
    }

    public Post getPreview(String link) {
        boolean image = false;

        if (!link.startsWith("http://") && !link.startsWith("https://")) {
            link = "http://" + link;
        }

        Post post = new Post();

        UrlValidator urlValidator = new UrlValidator();
        if (urlValidator.isValid(link)) {

            String protocol = "http";
            try {
                URL url = new URL(link);
                protocol = url.getProtocol();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            post.setLink(link);
            int slash = link.indexOf("/", 8);
            if (slash > 0) {
                post.setHomepage(link.substring(0, slash));
            } else {
                post.setHomepage(link);
            }
            Document page;
            String title = "";
            String imgUrl = null;
            String description = null;
            String root = link;

            try {
                String pageHtml = getText(link, 0);
                System.out.println("-------------------");
                System.out.println(pageHtml);
                System.out.println("-------------------");
                page = Jsoup.parse(pageHtml);
                Elements elems;

                if (root.contains("//")) {
                    int index = root.indexOf("//");
                    root = root.substring(index + 2);
                }
                if (root.contains("/")) {
                    root = root.substring(0, root.indexOf("/"));
                }

                if ((elems = page.select("meta[property=og:title]")) != null && elems.size() > 0) {
                    title = elems.first().attr("content");
                } else if ((elems = page.select("title")) != null && elems.size() > 0) {
                    title = elems.first().text();
                } else {
                    title = root;
                }

                if ((elems = page.select("meta[property=og:image]")) != null && elems.size() > 0) {
                    imgUrl = elems.first().attr("content");
                } else if ((elems = page.select("img")) != null && elems.size() > 0) {
                    for (Element img : elems) {

                        String iurl = img.attr("src");
                        if (!iurl.startsWith("http://") && !iurl.startsWith("https://") && !iurl.startsWith("ftp://")) {

                            if (iurl.startsWith("//")) {
                                iurl = "http:" + iurl;
                            } else {

                                if (!iurl.startsWith("/")) {
                                    iurl = "/" + iurl;
                                }
                                if (root.startsWith("www.")) {
                                    iurl = "http://" + root.substring(4) + iurl;
                                } else {
                                    iurl = "http://" + root + iurl;
                                }
                            }
                        }

                        if (getResponseCode(iurl, protocol) != 404) {
                            imgUrl = iurl;
                            break;
                        }

                    }

                }

                if ((elems = page.select("meta[property=og:description]")) != null && elems.size() > 0) {
                    description = elems.first().attr("content");
                } else if ((elems = page.select("meta[name=description]")) != null && elems.size() > 0) {
                    description = elems.first().attr("content");
                }

                if (title != null) {
                    if (title.length() > 70) {
                        title = title.substring(0, 65) + "...";
                    }
                    post.setTitle(title);
                }

                if (!image) {
                    if (description != null) {
                        if (description.length() > 180) {
                            description = description.substring(0, 175) + "...";
                        }
                        post.setDescription(description);
                    }

                    if (imgUrl != null && getResponseCode(imgUrl, protocol) != 404) {
                        post.setPhotoUrl(imgUrl);
                    }

                }
            } catch (org.jsoup.UnsupportedMimeTypeException | org.jsoup.HttpStatusException e) {
                e.printStackTrace();
                post.setTitle(link);
            } catch (Exception e) {
                e.printStackTrace();
                post = null;
            }

        } else {
            post = null;
        }
        return post;
    }
}
