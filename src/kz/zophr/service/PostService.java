/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.service;

import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import kz.zophr.entity.Category;
import kz.zophr.entity.Comment;
import kz.zophr.entity.Post;
import kz.zophr.entity.PostDetail;
import kz.zophr.entity.User;
import kz.zophr.entity.helper.PostCategory;
import kz.zophr.entity.helper.PostTag;
import kz.zophr.entity.helper.PostVote;
import org.springframework.stereotype.Service;

/**
 *
 * @author Madi
 */
@Service
public class PostService extends MainService {

    @Transactional
    public void save(Post post, String[] categories) {
        post.setSubmitDate(new Date());
        Post p = entityManager.merge(post);
        if (categories != null && categories.length > 0) {
            for (String category : categories) {
                PostCategory postCategory = new PostCategory();
                postCategory.setCategoryCode(category);
                postCategory.setPostId(p.getId());
                entityManager.persist(postCategory);
            }
        }
    }

    @Transactional
    public void save(Post post, String[] categories, String tags) {
        post.setSubmitDate(new Date());
        Post p = entityManager.merge(post);
        if (categories != null && categories.length > 0) {
            for (String category : categories) {
                Category cat = getCategory(category);
                PostCategory postCategory = new PostCategory();
                postCategory.setCategoryCode(cat.getCode());
                postCategory.setPostId(p.getId());
                postCategory.setCategory(cat);
                entityManager.persist(postCategory);
            }
        }
        if (tags != null) {
            String[] ts = tags.split(",");
            for (String tag : ts) {
                if (tag != null && tag.trim().length() > 0) {
                    PostTag postTag = new PostTag();
                    postTag.setPostId(p.getId());
                    postTag.setTag(tag);
                    entityManager.persist(postTag);
                }

            }
        }
    }

    @Transactional
    public void saveComment(User user, Long postId, String comment) {
        Comment comm = new Comment();
        comm.setPostId(postId);
        comm.setComment(comment);
        comm.setUserId(user.getId());
        comm.setAuthorName(user.getName());
        comm.setAuthorSurname(user.getSurname());
        comm.setCommentDate(new Date());
        entityManager.persist(comm);
    }

    public List<Category> getCategories() {
        TypedQuery<Category> query = entityManager.createQuery("select c from Category c order by c.order asc", Category.class);
        return query.getResultList();
    }

    public Category getCategory(String code) {
        TypedQuery<Category> query = entityManager.createQuery("select c from Category c where c.code = :code", Category.class);
        query.setParameter("code", code);
        return query.getSingleResult();
    }

    public List<Post> getPosts(String category) {
        TypedQuery<Post> query;
        if (category != null) {
            query = entityManager.createQuery("select p from Post p where p.id in (select pc.postId from PostCategory pc where pc.categoryCode = :categoryCode) order by p.id desc", Post.class);
            query.setParameter("categoryCode", category);
        } else {
            query = entityManager.createQuery("select p from Post p order by p.id desc", Post.class);
        }
        return query.getResultList();
    }

    public List<PostDetail> getPostDetails(String category, Integer first, Integer max) {
        TypedQuery<PostDetail> query;
        if (category != null) {
            query = entityManager.createQuery("select p from PostDetail p where p.archived = 0 and p.id in (select pc.postId from PostCategory pc where pc.categoryCode = :categoryCode) order by p.id desc", PostDetail.class);
            query.setParameter("categoryCode", category);
        } else {
            query = entityManager.createQuery("select p from PostDetail p where p.archived = 0 order by p.id desc", PostDetail.class);
        }

        if (max > 0) {
            query.setFirstResult(first);
            query.setMaxResults(max);
        }
        List<PostDetail> result = query.getResultList();

        return result;
    }

    public List<PostDetail> getPostDetailsByTag(String tag, Integer first, Integer max) {
        TypedQuery<PostDetail> query;
        if (tag != null) {
            query = entityManager.createQuery("select p from PostDetail p where p.archived = 0 and p.id in (select pt.postId from PostTag pt where pt.tag = :tag) order by p.id desc", PostDetail.class);
            query.setParameter("tag", tag);
        } else {
            query = entityManager.createQuery("select p from PostDetail p where p.archived = 0 order by p.id desc", PostDetail.class);
        }

        if (max > 0) {
            query.setFirstResult(first);
            query.setMaxResults(max);
        }
        List<PostDetail> result = query.getResultList();

        return result;
    }

    public List<String> getPostTags() {
        Query query = entityManager.createQuery("select distinct(p.tag) from PostTag p order by p.tag asc");
        return query.getResultList();
    }

    public PostDetail getPostDetail(Long postId) {
        TypedQuery<PostDetail> query;
        query = entityManager.createQuery("select p from PostDetail p where p.id = :postId and p.archived = 0", PostDetail.class);
        query.setParameter("postId", postId);
        return query.getSingleResult();
    }

    public boolean urlAlreadyExists(String url) {
        TypedQuery<Post> query;
        query = entityManager.createQuery("select p from Post p where p.link = :url and p.archived = 0", Post.class);
        query.setParameter("url", url);
        return query.getResultList().size() > 0;
    }

    public List<Comment> getPostComments(Long postId) {
        TypedQuery<Comment> query;
        query = entityManager.createQuery("select c from Comment c where c.postId = :postId and c.archived = 0 order by c.id asc", Comment.class);
        query.setParameter("postId", postId);
        return query.getResultList();
    }

    public List<PostDetail> getUserPosts(Long userId) {
        TypedQuery<PostDetail> query;
        query = entityManager.createQuery("select p from PostDetail p where p.authorId = :userId and p.archived = 0 order by p.id desc", PostDetail.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<Comment> getUserComments(Long userId) {
        TypedQuery<Comment> query;
        query = entityManager.createQuery("select c from Comment c where c.userId = :userId and c.archived = 0 order by c.id desc", Comment.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<Comment> getAllComments() {
        TypedQuery<Comment> query;
        query = entityManager.createQuery("select c from Comment c where c.archived = 0 order by c.id desc", Comment.class);
        return query.getResultList();
    }

    @Transactional
    public Boolean votePost(Long postId, Long userId, String vote) {
        Boolean changed = false;
        PostVote voted = getPostVote(postId, userId);
        if (voted != null) {
            if (!voted.getVote().equalsIgnoreCase(vote)) {
                voted.setArchived(1);
                entityManager.persist(voted);
                changed = true;
                PostVote postVote = new PostVote();
                postVote.setPostId(postId);
                postVote.setUserId(userId);
                postVote.setVote(vote);
                postVote.setVoteDate(new Date());
                postVote.setArchived(0);
                entityManager.persist(postVote);
            } else {
                changed = null;
            }
        } else {
            PostVote postVote = new PostVote();
            postVote.setPostId(postId);
            postVote.setUserId(userId);
            postVote.setVote(vote);
            postVote.setVoteDate(new Date());
            postVote.setArchived(0);
            entityManager.persist(postVote);
        }

        return changed;
    }

    public PostVote getPostVote(Long postId, Long userId) {
        TypedQuery<PostVote> query = entityManager.createQuery("select pv from PostVote pv where pv.userId = :userId and pv.postId = :postId and pv.archived = 0", PostVote.class);
        query.setParameter("userId", userId);
        query.setParameter("postId", postId);
        List<PostVote> list = query.getResultList();
        return (list != null && list.size() == 1) ? list.get(0) : null;
    }

    @Transactional
    public void removePost(Long postId) {
        TypedQuery<Post> query = entityManager.createQuery("select p from Post p where p.id = :postId", Post.class);
        query.setParameter("postId", postId);
        Post post = query.getSingleResult();
        post.setArchived(1L);
        entityManager.persist(post);
    }

    @Transactional
    public void removeComment(Long commentId) {
        TypedQuery<Comment> query = entityManager.createQuery("select c from Comment c where c.id = :commentId", Comment.class);
        query.setParameter("commentId", commentId);
        Comment comment = query.getSingleResult();
        comment.setArchived(1L);
        entityManager.persist(comment);
    }
}
