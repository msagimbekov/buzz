/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Madi
 */
@Service
public class MainService {
    
    @PersistenceContext
    public EntityManager entityManager;
    
    @Transactional
    public void persist(Object object) {
        entityManager.persist(object);
    }

    @Transactional
    public void merge(Object object) {
        entityManager.merge(object);
    }

    @Transactional
    public void remove(Object entity) {
        entityManager.remove(entity);
    }

    @Transactional
    public void refresh(Object entity) {
        entityManager.refresh(entity);
    }

    @Transactional
    public void removeDetached(Object object, Long id) {
        entityManager.remove(entityManager.getReference(object.getClass(), id));
    }
}
