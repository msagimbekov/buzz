/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.service;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import kz.zophr.entity.Role;
import kz.zophr.entity.User;
import kz.zophr.entity.helper.UserRole;
import org.springframework.stereotype.Service;

/**
 *
 * @author Madi
 */
@Service
public class UserService extends MainService {

    @Transactional
    public User save(User user) {
        return entityManager.merge(user);
    }

    @Transactional
    public User getUser(String uid, String network) {
        TypedQuery<User> query = entityManager.createQuery("select u from User u where u.uid = :uid and u.socialNetwork = :network", User.class);
        query.setParameter("uid", uid);
        query.setParameter("network", network);
        List<User> list = query.getResultList();
        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        return null;
    }
    
    @Transactional
    public User getUser(Long userId) {
        TypedQuery<User> query = entityManager.createQuery("select u from User u where u.id = :id", User.class);
        query.setParameter("id", userId);
        List<User> list = query.getResultList();
        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        return null;
    }
    
    @Transactional
    public UserRole getUserRole(Long userId) {
        TypedQuery<UserRole> query = entityManager.createQuery("select u from UserRole u where u.userId = :userId", UserRole.class);
        query.setParameter("userId", userId);
        return query.getSingleResult();
    }
    
    @Transactional
    public void setUserRole(Long userId, Long roleId) {
        UserRole userRole = new UserRole();
        userRole.setRoleId(roleId);
        userRole.setUserId(userId);
        entityManager.persist(userRole);
    }
    
    public Role getUserStatus(Long userId) {
        UserRole userRole = getUserRole(userId);
        TypedQuery<Role> query = entityManager.createQuery("select r from Role r where r.id = :roleId", Role.class);
        query.setParameter("roleId", userRole.getRoleId());
        return query.getSingleResult();
    }

}
