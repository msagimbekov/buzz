/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.controller;

import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import kz.zophr.entity.Comment;
import kz.zophr.entity.Post;
import kz.zophr.entity.PostDetail;
import kz.zophr.entity.User;
import kz.zophr.entity.helper.UserRole;
import kz.zophr.helper.SourceContent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Madi
 */
@Controller
public class PageController extends MainController {

    @RequestMapping("/staffonly")
    public String staffPage(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            UserRole role = userService.getUserRole(user.getId());
            if (role.getRoleId() == 1 || role.getRoleId() == 2) {
                return "staffonly";
            }
        }
        return "redirect:/list";
    }

    @RequestMapping("/staffonly-posts")
    public String staffPostsPage(HttpSession session, Map<String, Object> map) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            UserRole role = userService.getUserRole(user.getId());
            if (role.getRoleId() == 1 || role.getRoleId() == 2) {
                List<PostDetail> list = postService.getPostDetails(null, 0, -1);
                map.put("posts", list);
                return "staffonly-posts";
            }
        }
        return "redirect:/list";
    }

    @RequestMapping("/staffonly-comments")
    public String staffCommentsPage(HttpSession session, Map<String, Object> map) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            UserRole role = userService.getUserRole(user.getId());
            if (role.getRoleId() == 1 || role.getRoleId() == 2) {
                List<Comment> comments = postService.getAllComments();
                map.put("comments", comments);
                return "staffonly-comments";
            }
        }
        return "redirect:/list";
    }

    @RequestMapping(value = "/remove-post", method = RequestMethod.POST)
    public String removePost(@RequestParam("postId") Long postId, HttpSession session, Map<String, Object> map) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            UserRole role = userService.getUserRole(user.getId());
            if (role.getRoleId() == 1 || role.getRoleId() == 2) {
                postService.removePost(postId);
                List<PostDetail> list = postService.getPostDetails(null, 0, -1);
                map.put("posts", list);
                return "staffonly-posts";
            }
        }
        return "redirect:/list";
    }

    @RequestMapping("/remove-comment")
    public String removeComment(@RequestParam("commentId") Long commentId, HttpSession session, Map<String, Object> map) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            UserRole role = userService.getUserRole(user.getId());
            if (role.getRoleId() == 1 || role.getRoleId() == 2) {
                postService.removeComment(commentId);
                List<Comment> comments = postService.getAllComments();
                map.put("comments", comments);
                return "staffonly-comments";
            }
        }
        return "redirect:/list";
    }

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session, Map<String, Object> map, HttpServletRequest request) {
        session.invalidate();
        String contextPath = request.getContextPath();
        String referer = request.getHeader("referer");
        return "redirect:/list";
    }

    @RequestMapping("/profile/{userId}")
    public String profilePage(@PathVariable("userId") Long userId, Map<String, Object> map) {
        map.put("profile", userService.getUser(userId));
        List<PostDetail> posts = postService.getUserPosts(userId);
        List<Comment> comments = postService.getUserComments(userId);
        map.put("postCount", posts.size());
        map.put("commentCount", comments.size());

        Long likes = 0L;
        Long dislikes = 0L;

        for (PostDetail detail : posts) {
            likes += detail.getLikes();
            dislikes += detail.getDislikes();
        }

        map.put("likes", likes);
        map.put("dislikes", dislikes);
        map.put("status", userService.getUserStatus(userId).getName());

        return "profile";
    }

    @RequestMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping("/add-link")
    public String addLinkPage() {
        return "add-link";
    }

    /*@RequestMapping("/list")
     public String listPage(@RequestParam(value = "category", required = false) String category, Map<String, Object> map, HttpSession session) {
     map.put("posts", postService.getPosts(category));
     return "list";
     }*/
    @RequestMapping("/list")
    public String listPage(Map<String, Object> map, HttpSession session) {
        session.setAttribute("category", null);
        session.setAttribute("tag", null);
        List<PostDetail> list = postService.getPostDetails(null, 0, size);
        map.put("posts", list);
        map.put("posts_size", list.size());
        return "list";
    }
    
    @RequestMapping("/tag/{tag}")
    public String listByTag(@PathVariable("tag") String tag, Map<String, Object> map, HttpSession session) {
        session.setAttribute("category", null);
        session.setAttribute("tag", tag);
        List<PostDetail> list = postService.getPostDetailsByTag(tag, 0, size);
        map.put("posts", list);
        map.put("posts_size", list.size());
        return "list";
    }

    @RequestMapping("/list/{category}")
    public String listCategoryPage(@PathVariable("category") String category, Map<String, Object> map, HttpSession session) {
        session.setAttribute("category", category);
        session.setAttribute("tag", null);
        List<PostDetail> list = postService.getPostDetails(category, 0, size);
        map.put("posts", list);
        map.put("posts_size", list.size());
        return "list";
    }

    @RequestMapping("/list-ajax")
    public @ResponseBody
    String listAjaxLoad(@RequestParam("first") Integer first, Map<String, Object> map, HttpSession session) {
        String category = (String) session.getAttribute("category");
        String tag = (String) session.getAttribute("tag");
        List<PostDetail> list;
        if (tag != null) {
            list = postService.getPostDetailsByTag(tag, first, size);
        } else {
            list = postService.getPostDetails(category, first, size);
        }
        return new Gson().toJson(list);
    }

    @RequestMapping("/comments/{postId}")
    public String commentsPage(@PathVariable("postId") Long postId, Map<String, Object> map, HttpSession session, HttpServletRequest request) {
        map.put("post", postService.getPostDetail(postId));
        map.put("comments", postService.getPostComments(postId));
        request.setAttribute("post", postId);
        return "comments";
    }

    @RequestMapping("/link-preview-test")
    public String linkPreviewTest(@RequestParam(value = "url", required = false) String url, Map<String, Object> map) {
        map.put("url", url);
        if (url != null && !url.equalsIgnoreCase("")) {
            Post post = linkService.getPreview(url);
            if (post != null) {
                map.put("post", post);
                map.put("categories", postService.getCategories());
            } else {
                map.put("error", "Невалидная ссылка");
            }
        } else {
            map.put("error", "Заполните поле");
        }
        return "add-link";
    }

    @RequestMapping("/link-preview")
    public String linkPreview(@RequestParam(value = "url", required = false) String url, Map<String, Object> map) {
        map.put("url", url);
        if (url != null && !url.equalsIgnoreCase("")) {
            SourceContent sourceContent = webViewService.test(url);
            if (sourceContent != null) {
                Post post = new Post();
                post.setHomepage(sourceContent.getCannonicalUrl());
                post.setTitle(sourceContent.getTitle());
                post.setDescription(sourceContent.getDescription());
                post.setLink(sourceContent.getUrl());
                if (sourceContent.getImages() != null && sourceContent.getImages().size() > 0) {
                    post.setPhotoUrl(sourceContent.getImages().get(0));
                }

                map.put("post", post);
                map.put("categories", postService.getCategories());
            } else {
                map.put("error", "Невалидная ссылка");
            }
        } else {
            map.put("error", "Заполните поле");
        }
        return "add-link";
    }

    @RequestMapping("/save-link")
    public String saveLink(@RequestParam(value = "url", required = false) String url, @RequestParam(value = "category", required = false) String[] categories, @RequestParam(value = "tags", required = false) String tags, Map<String, Object> map, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (url != null && !url.equalsIgnoreCase("")) {
            boolean exists = postService.urlAlreadyExists(url);
            if (!exists) {

                SourceContent sourceContent = webViewService.test(url);
                if (sourceContent != null) {
                    Post post = new Post();
                    post.setHomepage(sourceContent.getCannonicalUrl());
                    post.setTitle(sourceContent.getTitle());
                    post.setDescription(sourceContent.getDescription());
                    post.setLink(sourceContent.getUrl());
                    if (sourceContent.getImages() != null && sourceContent.getImages().size() > 0) {
                        post.setPhotoUrl(sourceContent.getImages().get(0));
                    }

                    post.setAuthorId(user.getId());
                    post.setAuthorName(user.getName());
                    post.setAuthorSurname(user.getSurname());
                    postService.save(post, categories, tags);
                    return "redirect:/list";
                } else {
                    map.put("url", url);
                    map.put("error", "Невалидная ссылка");
                    return "add-link";
                }
            } else {
                map.put("url", url);
                map.put("error", "Ссылка уже была добавлена");
                return "add-link";
            }
        } else {
            map.put("url", url);
            map.put("error", "Заполните поле");
            return "add-link";
        }
    }
}
