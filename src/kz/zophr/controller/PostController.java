/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.controller;

import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpSession;
import kz.zophr.entity.Comment;
import kz.zophr.entity.User;
import kz.zophr.entity.helper.PostVote;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Madi
 */
@Controller
public class PostController extends MainController {
    
    @RequestMapping(value = "/like", method = RequestMethod.POST)
    public @ResponseBody String likePost(@RequestParam("postId") Long postId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            Boolean liked = postService.votePost(postId, user.getId(), PostVote.LIKE);
            if (liked == null) {
                return "NOTHING";
            } else {
                return liked ? "CHANGED" : "NEW"; 
            }
        } else {
            return "AUTHORIZE";
        }
    }
    
    @RequestMapping(value = "/dislike", method = RequestMethod.POST)
    public @ResponseBody String dislikePost(@RequestParam("postId") Long postId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            Boolean disliked = postService.votePost(postId, user.getId(), PostVote.DISLIKE);
            if (disliked == null) {
                return "NOTHING";
            } else {
                return disliked ? "CHANGED" : "NEW"; 
            }
        } else {
            return "AUTHORIZE";
        }
    }
    
    @RequestMapping(value = "/save-comment", method = RequestMethod.POST)
    public String saveComment(@RequestParam("post") Long postId, @RequestParam("comment") String comment, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user != null && comment != null && comment.length() < 500) {
            postService.saveComment(user, postId, Jsoup.parse(comment.replaceAll("\r\n", "@!#@#!")).text().replaceAll("@!#@#!", "<br/>"));
        } 
        return "redirect:/comments/" + postId;
    }
    
    @RequestMapping(value = "/tags")
    public @ResponseBody
    String getTags(Map<String, Object> map, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return null;
        } 
        return new Gson().toJson(postService.getPostTags());
    }
    
}
