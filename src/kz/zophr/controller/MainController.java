/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.controller;

import kz.zophr.service.LinkService;
import kz.zophr.service.PostService;
import kz.zophr.service.WebViewService;
import kz.zophr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Madi
 */
@Controller
public class MainController {
      
    protected static final Integer size = 10;
    
    @Autowired
    protected UserService userService;
    
    @Autowired
    protected LinkService linkService;
    
    @Autowired
    public PostService postService;
    
    @Autowired
    public WebViewService webViewService;
    
}
