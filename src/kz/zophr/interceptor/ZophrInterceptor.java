/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.interceptor;

import com.google.gson.Gson;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import kz.zophr.entity.User;
import kz.zophr.entity.helper.SocialUser;
import kz.zophr.service.PostService;
import kz.zophr.service.UserService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Madi
 */
public class ZophrInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    protected UserService userService;
    
    @Autowired
    protected PostService postService;
    
    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response, Object handler) throws MalformedURLException, IOException {
        
        HttpSession session = request.getSession();
        if (session.getAttribute("categoryList") == null) {
            session.setAttribute("categoryList", postService.getCategories());
        }
        
        if (request.getParameter("token") != null) {
            URL url = new URL("http://ulogin.ru/token.php?token=" + request.getParameter("token") + "&host=" + request.getServerName());
            String content = IOUtils.toString(url.openStream(), "UTF-8");
            Gson gson = new Gson();
            SocialUser socialUser = gson.fromJson(content, SocialUser.class);
            
            User user = userService.getUser(socialUser.getUid(), socialUser.getNetwork());
            if (user == null) {
                user = new User();
                user.setUsername(socialUser.getIdentity());
                user.setName(socialUser.getFirst_name());
                user.setProfile(socialUser.getProfile());
                user.setRegistrationDate(new Date());
                user.setSocialNetwork(socialUser.getNetwork());
                user.setSurname(socialUser.getLast_name());
                user.setUid(socialUser.getUid());
                User savedUser = userService.save(user);
                request.getSession().setAttribute("user", savedUser);
                userService.setUserRole(savedUser.getId(), 3L);
            } else {
                request.getSession().setAttribute("user", user);
            }
        }
        // your logic
        return true;
    }
}
