/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.zophr.entity.helper;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Madi
 */
@Entity
@Table(name = "POST_VOTES")
public class PostVote implements Serializable {

    public static String LIKE = "LIKE";
    public static String DISLIKE = "DISLIKE";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POST_VOTE_ID")
    private Long id;
    
    @Column(name = "POST_ID")
    private Long postId;
    
    @Column(name = "USER_ID")
    private Long userId;
    
    @Column(name = "VOTE")
    private String vote;
    
    @Column(name = "VOTE_DATE")
    private Date voteDate;
    
    @Column(name = "ARCHIVED")
    private Integer archived;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public Date getVoteDate() {
        return voteDate;
    }

    public void setVoteDate(Date voteDate) {
        this.voteDate = voteDate;
    }

    public Integer getArchived() {
        return archived;
    }

    public void setArchived(Integer archived) {
        this.archived = archived;
    }
    
}
