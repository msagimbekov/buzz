package kz.zophr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import kz.zophr.entity.helper.PostCategory;
import kz.zophr.entity.helper.PostTag;

@Entity
@Table(name = "POST_DETAILS")
public class PostDetail implements Serializable {

    @Id
    @Column(name = "POST_ID")
    private Long id;

    @Column(name = "LINK")
    private String link;

    @Column(name = "TITLE")
    private String title;
    
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    @Column(name = "HOMEPAGE")
    private String homepage;

    @Column(name = "PHOTO_URL")
    private String photoUrl;

    @Column(name = "AUTHOR_NAME")
    private String authorName;
    
    @Column(name = "AUTHOR_SURNAME")
    private String authorSurname;
    
    @Column(name = "AUTHOR_ID")
    private Long authorId;
    
    @Column(name = "LIKES")
    private Long likes;
    
    @Column(name = "DISLIKES")
    private Long dislikes;
    
    @Column(name = "COMMENTS") 
    private Long comments;
    
    @Column(name = "ARCHIVED")
    private Long archived = 0L;
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "POST_ID", nullable = true)
    private List<PostTag> tags;
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "POST_ID", nullable = true)
    private List<PostCategory> categories;

    public Long getArchived() {
        return archived;
    }

    public void setArchived(Long archived) {
        this.archived = archived;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getDislikes() {
        return dislikes;
    }

    public void setDislikes(Long dislikes) {
        this.dislikes = dislikes;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public List<PostTag> getTags() {
        return tags;
    }

    public void setTags(List<PostTag> tags) {
        this.tags = tags;
    }

    public List<PostCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<PostCategory> categories) {
        this.categories = categories;
    }
}
