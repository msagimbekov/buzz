<%-- 
    Document   : list
    Created on : 26.03.2015, 15:11:56
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<script>

    var list_size = ${posts_size};
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            $.ajax({
                url: '<%=request.getContextPath()%>/list-ajax',
                data: 'first=' + list_size,
                method: 'POST',
                dataType: 'json',
                success: function (data) {
                    var items = '';
                    for (var i = 0; i < data.length; i++) {
                        var d = new Date(data[i].submitDate);
                        var dDate = d.getDate();
                        var dMonth = d.getMonth() + 1;
                        var dHours = d.getHours();
                        var dMins = d.getMinutes();
                        items += '<div class="row">'
                                + '<section class="9u 12u(narrower)">'
                                + '<div class="box post" style="border: 1px dotted grey; padding: 10px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">'
                                + '<a href="' + data[i].link + '" target="_blank" class="image left"><img src="' + data[i].photoUrl + '" style="max-height: 100px;" alt="" /></a>'
                                + '<div class="inner">'
                                + '<h4 style="margin-bottom: 4px; line-height: 140%"><a href="' + data[i].link + '" target="_blank">' + data[i].title + '</a> [<a href="' + data[i].homepage + '" target="_blank">' + data[i].homepage + '</a>]</h4>'
                                + data[i].description + '<div style="margin-top: 5px;"><div/>'
                                + '<a href="<%=request.getContextPath()%>/comments/' + data[i].id + '"><img src="<%=request.getContextPath()%>/images/comments.png" title="Комментарий" style="max-width: 15px; display: inline;"/> ' + data[i].comments + '</a> | <a href="javascript:like(' + data[i].id + ')"><img src="<%=request.getContextPath()%>/images/like.png" title="Нравится" style="max-width: 12px; display: inline;"/> <span id="likes' + data[i].id + '">' + data[i].likes + '</span></a>, <a href="javascript:dislike(' + data[i].id + ')" style="white-space:nowrap"><img src="<%=request.getContextPath()%>/images/dislike.png" title="Не нравится" style="max-width: 12px; display: inline;"/> <span id="dislikes' + data[i].id + '">' + data[i].dislikes + '</span></a> | <a href="<%=request.getContextPath()%>/profile/' + data[i].authorId + '" style="white-space:nowrap">' + data[i].authorName + ' ' + data[i].authorSurname + '</a> | ' + (dDate < 10 ? '0' : '' ) + dDate + '.' + (dMonth < 10 ? '0' : '' ) + dMonth + '.' + d.getFullYear() + ' ' + (dHours < 10 ? '0' : '') + dHours + ":" + (dMins < 10 ? '0' : '') + dMins;                           
                                
                                if (data[i].tags.length > 0) {
                                    items += '<h4 style="margin-bottom: 0px;"><img src="<%=request.getContextPath()%>/images/tag-icon.png" title="Теги" style="max-width: 12px; display: inline;"/> ';
                                    for (var j = 0; j < data[i].tags.length; j++) {
                                        items += '<a href="<%=request.getContextPath()%>/tag/' + data[i].tags[j].tag + '" style="text-decoration: underline; font-weight: normal; color: grey;">' + data[i].tags[j].tag + '<a/> ';
                                    } 
                                    items += '</h4>';
                                }
                                
                                if (data[i].categories.length > 0) {
                                    items += '<h4><img src="<%=request.getContextPath()%>/images/category-icon.png" title="Категории" style="max-width: 12px; display: inline;"/> ';
                                    for (var j = 0; j < data[i].categories.length; j++) {
                                        items += '<a href="<%=request.getContextPath()%>/list/' + data[i].categories[j].category.code + '" style="text-decoration: underline; font-weight: normal; color: grey;">' + data[i].categories[j].category.name + '<a/> ';
                                    } 
                                    items += '</h4>';
                                }
                                
                                items += '</div>'
                                + '</div>'
                                + '</section>'
                                + '</div>';
                    }    
                    
                    $("div.container").append(items);
                    
                    list_size = list_size + data.length;
                    
                },
                error: function (e) {

                }
            });
        }
    });
</script>
<style>

    .row {
        margin-top: 0px !important;
        padding-top: 0px !important;
        padding-bottom: -50px !important;
        margin-bottom: 0px !important;
    }

    section {
        padding-top: 5px !important;
        //border-bottom: 1px dotted grey !important;
    }
    
</style>
<section class="wrapper style1">
    <div class="container">
        <c:if test="${!empty posts}">
            <c:forEach items="${posts}" var="post" varStatus="loop">
                <div class="row">
                    <section class="9u 12u(narrower)">
                        <div class="box post" style="border: 1px dotted grey; padding: 5px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">
                            <a href="${post.link}" target="_blank" class="image left"><img src="${post.photoUrl}" style="max-height: 100px;" alt="" /></a>
                            <div class="inner" >
                                <h4 style="margin-bottom: 4px; line-height: 140%"><a href="${post.link}" target="_blank">${post.title}</a> [<a href="${post.homepage}" target="_blank">${post.homepage}</a>]</h4>
                                ${post.description}<div style="margin-top: 5px;"><div/>
                                <a href="<%=request.getContextPath()%>/comments/${post.id}"><img src="<%=request.getContextPath()%>/images/comments.png" title="Комментарий" style="max-width: 15px; display: inline;"/> ${post.comments}</a> | <a href="javascript:like(${post.id})"><img src="<%=request.getContextPath()%>/images/like.png" title="Нравится" style="max-width: 12px; display: inline;"/> <span id="likes${post.id}">${post.likes}</span></a>, <a href="javascript:dislike(${post.id})" style="white-space:nowrap"><img src="<%=request.getContextPath()%>/images/dislike.png" title="Не нравится" style="max-width: 12px; display: inline;"/> <span id="dislikes${post.id}">${post.dislikes}</span></a> | <a href="<%=request.getContextPath()%>/profile/${post.authorId}" style="white-space:nowrap">${post.authorName} ${post.authorSurname}</a> | <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${post.submitDate}" />
                                <c:if test="${!empty post.tags}">
                                    <c:if test="${fn:length(post.tags) gt 0}">
                                    <h4 style="margin-bottom: 0px;"><img src="<%=request.getContextPath()%>/images/tag-icon.png" title="Теги" style="max-width: 12px; display: inline;"/>
                                            <c:forEach items="${post.tags}" var="tag">
                                                <a href="<%=request.getContextPath()%>/tag/${tag.tag}" style="text-decoration: underline; font-weight: normal; color: grey; display: inline;">${tag.tag}</a>
                                            </c:forEach>
                                        </h4>
                                    </c:if>
                                </c:if>  
                                <c:if test="${!empty post.categories}">
                                    <c:if test="${fn:length(post.categories) gt 0}">
                                        <h4><img src="<%=request.getContextPath()%>/images/category-icon.png" title="Категории" style="max-width: 12px; display: inline;"/>
                                            <c:forEach items="${post.categories}" var="category">
                                                <a href="<%=request.getContextPath()%>/list/${category.category.code}" style="text-decoration: underline; font-weight: normal; color: grey;">${category.category.name}</a>
                                            </c:forEach>
                                        </h4>
                                    </c:if>
                                </c:if>
                            </div>
                        </div>
                    </section>
                </div>
            </c:forEach>
        </c:if>      
    </div>
</section>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>