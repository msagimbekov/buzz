<%-- 
    Document   : comments
    Created on : 01.05.2015, 11:20:21
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<script>
    var title = $("title");
    title.text("${fn:replace(post.title, "\"", "")} - " + title.text());
</script>
<style>

    .row {
        margin-top: 0px !important;
        padding-top: 0px !important;
        padding-bottom: -50px !important;
        margin-bottom: 0px !important;
    }

    section {
        padding-top: 5px !important;
        //border-bottom: 1px dotted grey !important;
    }

    .yashare-auto-init span {
        padding-left: 0px !important;
        margin-left: 0px !important;
        float: left;
    }

</style>
<section class="wrapper style1">
    <div class="container">
        <div class="row">
            <section class="9u 12u(narrower)">
                <div class="box post"style="border: 1px dotted grey; padding: 5px 0 10px 10px !important; margin: 0px !important; border-radius: 5px; padding-bottom: 15px !important; margin-bottom: 15px !important;">
                    <a href="${post.link}" target="_blank" class="image left"><img src="${post.photoUrl}" style="max-height: 100px;" alt="" /></a>
                    <div class="inner">
                        <h4 style="margin-bottom: 4px; line-height: 140%"><a href="${post.link}" target="_blank">${post.title}</a> [<a href="${post.homepage}" target="_blank">${post.homepage}</a>]</h4>
                        ${post.description}<div style="margin-top: 5px;"><div/>
                        <a href="<%=request.getContextPath()%>/comments/${post.id}"><img src="<%=request.getContextPath()%>/images/comments.png" title="Комментарий" style="max-width: 15px; display: inline;"/> ${post.comments}</a> | <a href="javascript:like(${post.id})"><img src="<%=request.getContextPath()%>/images/like.png" title="Нравится" style="max-width: 12px; display: inline;"/> <span id="likes${post.id}">${post.likes}</span></a>, <a href="javascript:dislike(${post.id})" style="white-space:nowrap"><img src="<%=request.getContextPath()%>/images/dislike.png" title="Не нравится" style="max-width: 12px; display: inline;"/> <span id="dislikes${post.id}">${post.dislikes}</span></a> | <a href="<%=request.getContextPath()%>/profile/${post.authorId}" style="white-space:nowrap">${post.authorName} ${post.authorSurname}</a> | <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${post.submitDate}" />

                        <c:if test="${!empty post.tags}">
                            <h4 style="margin-bottom: 0px;"><img src="<%=request.getContextPath()%>/images/tag-icon.png" title="Теги" style="max-width: 12px; display: inline;"/>
                                <c:forEach items="${post.tags}" var="tag">
                                    <a href="<%=request.getContextPath()%>/tag/${tag.tag}" style="text-decoration: underline; font-weight: normal; color: grey; display: inline;">${tag.tag}</a>
                                </c:forEach>
                            </h4>
                        </c:if>  
                        <c:if test="${!empty post.categories}">
                            <c:if test="${fn:length(post.categories) gt 0}">
                                <h4><img src="<%=request.getContextPath()%>/images/category-icon.png" title="Категории" style="max-width: 12px; display: inline;"/>
                                    <c:forEach items="${post.categories}" var="category">
                                        <a href="<%=request.getContextPath()%>/list/${category.category.code}" style="text-decoration: underline; font-weight: normal; color: grey;">${category.category.name}</a>
                                    </c:forEach>
                                </h4>
                            </c:if>
                        </c:if>
                        
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,moimir,gplus"></div>
                    </div>
                </div>
            </section>
        </div>
        <c:if test="${user == null}">
            <h3><a href="<%=request.getContextPath()%>/login" style="text-decoration: underline;">Авторизуйтесь</a>, чтобы оставить комментарий</h3>
        </c:if>
        <c:if test="${user != null}">
            <form action="<%=request.getContextPath()%>/save-comment?post=${post.id}" method="POST">
                <div class="row">
                    <section class="9u 12u(narrower)">
                        <div class="box post">
                            <div class="12u">
                                <textarea name="comment" id="comment" placeholder="Написать комментарий" rows="2"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <section class="9u 12u(narrower)">
                        <div class="box post">
                            <div class="12u">
                                <ul class="actions">
                                    <li><input type="submit" class="button alt" value="Отправить" /></li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </form>
        </c:if>
        <c:if test="${!empty comments}">
            <c:forEach items="${comments}" var="comment" varStatus="loop">
                <div class="row">
                    <section class="9u 12u(narrower)">
                        <div class="box post" style="border: 1px dotted grey; padding: 10px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">
                            <a href="<%=request.getContextPath()%>/profile/${comment.userId}" style="white-space:nowrap">${comment.authorName} ${comment.authorSurname}</a> | <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${comment.commentDate}" /><br/>
                            <div style="margin-top: 5px; margin-left: 5px;">
                                <i>${comment.comment}</i>
                            </div>
                        </div>
                    </section>
                </div>
            </c:forEach>
        </c:if>
    </div>
</section>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-63279983-1', 'auto');
    ga('send', 'pageview');

</script>




