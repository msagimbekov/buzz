<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<section class="wrapper style1">
    <div class="container">
        <div id="content">
            <h3>Регистрация [<a href="<%=request.getContextPath()%>/login">авторизация</a>]</h3>
            <form>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="text" name="username" id="username" placeholder="Логин" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="password" name="password" id="password" placeholder="Пароль" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="password" name="confirm_password" id="confirm_password" placeholder="Подтвердите пароль" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="email" name="email" id="email" placeholder="Email" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" class="button alt" value="Зарегистрироваться" /></li>
                        </ul>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>

