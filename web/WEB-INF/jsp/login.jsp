<%-- 
    Document   : login
    Created on : 26.03.2015, 14:34:48
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="//ulogin.ru/js/ulogin.js"></script>
<!DOCTYPE html>

<section class="wrapper style1">
    <div class="container">
        <div id="content">
            <h3>Авторизация через социальные сети</h3>
            <div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name;providers=googleplus,facebook,vkontakte,mailru;redirect_uri=${header.referer}"></div>
            <!--hr>
            <form>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="text" name="username" id="username" placeholder="Логин" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="password" name="password" id="password" placeholder="Пароль" />
                    </div>
                </div>
                <div class="row 100%">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" class="button alt" value="Войти" /></li>
                        </ul>
                    </div>
                </div>
            </form-->

        </div>
    </div>
</section>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>

