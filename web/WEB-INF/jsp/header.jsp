<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<div id="header">

    <!-- Logo -->
    <h1><a href="<%=request.getContextPath()%>" id="logo">buzz.kz</a><span style="font-size: 7px">[beta]</span></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul class="left">
            <!-- class="current"-->
            <li><a href="<%=request.getContextPath()%>/list">Все</a></li>
                <c:if test="${!empty categoryList}">
                    <c:forEach items="${categoryList}" var="category">
                        <c:if test="${category.isMain == 'YES'}">
                        <li><a href="<%=request.getContextPath()%>/list/${category.code}">${category.name}</a></li>
                        </c:if>
                    </c:forEach>
                </c:if>
            <li>
                <a href="">...</a>
                <ul>
                    <c:if test="${!empty categoryList}">
                        <c:forEach items="${categoryList}" var="category">
                            <c:if test="${category.isMain == 'NO'}">
                                <li><a href="<%=request.getContextPath()%>/list/${category.code}">${category.name}</a></li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                </ul>
            </li>
            <li> <span style="color: #999; font-size: 20px; font-weight: bold">|</span> </li>
            <li><a href="<%=request.getContextPath()%>/add-link">Добавить ссылку</a></li>
                <c:if test="${user == null}">
                <li><a href="<%=request.getContextPath()%>/login">Войти</a></li>
                </c:if>
                <c:if test="${user != null}">
                <li><a href="<%=request.getContextPath()%>/profile/${user.id}">${user.name} ${user.surname}</a></li>
                <li><a href="<%=request.getContextPath()%>/logout">[Выйти]</a></li>
                </c:if>
        </ul>
    </nav>

</div>    