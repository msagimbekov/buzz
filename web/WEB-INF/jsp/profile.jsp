<%-- 
    Document   : profile
    Created on : 03.04.2015, 23:00:41
    Author     : Madi
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<section class="wrapper style1">
    <div class="container">
        <div id="content">
            <b>Юзер:</b><br/>
            &nbsp;&nbsp; ${profile.name} ${profile.surname}<br/><br/>
            <b>Статус:</b><br/>
            &nbsp;&nbsp; ${status}<br/><br/>
            <b>Социальная сеть:</b><br/>
            &nbsp;&nbsp; ${profile.socialNetwork}<br/><br/>
            <b>Количество постов:</b><br/>
            &nbsp;&nbsp; ${postCount}<br/><br/>
            <b>Количество комментариев:</b><br/>
            &nbsp;&nbsp; ${commentCount}<br/><br/>
            <b>Интересность постов:</b><br/>
            &nbsp;&nbsp; "Нравится": ${likes}, "Не нравится": ${dislikes}<br/><br/>
        </div>
    </div>
</section>
        
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>

