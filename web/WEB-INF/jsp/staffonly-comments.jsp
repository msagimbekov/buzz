<%-- 
    Document   : staffonly_comments
    Created on : 08.05.2015, 18:53:05
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${!empty comments}">
            <c:forEach items="${comments}" var="comment" varStatus="loop">
                <div>
                    <form method="POST" action="<%=request.getContextPath()%>/remove-comment">
                        <div style="border: 1px dotted grey; padding: 10px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">
                            <a href="<%=request.getContextPath()%>/profile/${comment.userId}" style="white-space:nowrap">${comment.authorName} ${comment.authorSurname}</a> | <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${comment.commentDate}" /> | ${comment.postId} <br/>
                            ${comment.comment}
                        </div>
                        <input type="hidden" value="${comment.id}" name="commentId"/>
                        <input type="submit" value="remove"/>
                    </form>
                </div>
            </c:forEach>
        </c:if>
    </body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>
