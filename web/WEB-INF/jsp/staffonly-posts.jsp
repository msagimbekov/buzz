<%-- 
    Document   : staffonly-posts
    Created on : 08.05.2015, 18:52:46
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${!empty posts}">
            <c:forEach items="${posts}" var="post" varStatus="loop">
                <div>
                    <form method="POST" action="<%=request.getContextPath()%>/remove-post">
                        <div style="border: 1px dotted grey; padding: 5px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">
                            <a href="${post.link}" target="_blank">${post.title}</a>
                            | <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${post.submitDate}" /> | <a href="<%=request.getContextPath()%>/profile/${post.authorId}" style="white-space:nowrap">${post.authorName} ${post.authorSurname}</a>                           
                            <input type="hidden" value="${post.id}" name="postId"/>
                            <input type="submit" value="remove"/>
                        </div>
                    </form>
                </div>
            </c:forEach>
        </c:if>   
    </body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>
