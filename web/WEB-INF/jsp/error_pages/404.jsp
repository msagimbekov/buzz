<%-- 
    Document   : error
    Created on : 07.05.2015, 16:33:35
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404 - BUZZ.KZ</title>
    </head>
    <body>
        <div style="margin: 0 auto; text-align: center; margin-top: 100px; font-family: Verdana; color: #AAA">
            <h3>Страница не найдена!</h3>
        </div>
    </body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63279983-1', 'auto');
  ga('send', 'pageview');

</script>
