<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="footer">
    <div class="container">
        <div class="row">
            <section class="3u 6u(narrower) 12u(mobilep)">
                <h3>Links to Stuff</h3>
                <ul class="links">
                    <li><a href="#">Mattis et quis rutrum</a></li>
                    <li><a href="#">Suspendisse amet varius</a></li>
                </ul>
            </section>
            <section class="3u 6u(narrower) 12u(mobilep)">
                <h3>More Links to Stuff</h3>
                <ul class="links">
                    <li><a href="#">Duis neque nisi dapibus</a></li>
                    <li><a href="#">Sed et dapibus quis</a></li>
                    <li><a href="#">Rutrum accumsan sed</a></li>
                </ul>
            </section>
            <section class="6u 12u(narrower)">
                <h3>Get In Touch</h3>
                <form>
                    <div class="row 50%">
                        <div class="6u 12u(mobilep)">
                            <input type="text" name="name" id="name" placeholder="Имя" />
                        </div>
                        <div class="6u 12u(mobilep)">
                            <input type="email" name="email" id="email" placeholder="Email" />
                        </div>
                    </div>
                    <div class="row 50%">
                        <div class="12u">
                            <textarea name="message" id="message" placeholder="Сообщение" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="row 50%">
                        <div class="12u">
                            <ul class="actions">
                                <li><input type="submit" class="button alt" value="Отправить сообщение" /></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>

    <!-- Icons -->
    <ul class="icons">
        <li><a class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; 2015 Bytecode</li><li><a href="<%=request.getContextPath()%>">www.buzz.kz</a></li>
        </ul>
    </div>

</div>
