<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
    <head>
        <title>buzz.kz</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <meta name="description" content="">
        <meta name="robots" content="index, follow">
    </head>
    <body>
        <tiles:insertAttribute name="body" />
    </body>
</html>
