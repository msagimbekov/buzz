<%-- 
    Document   : add-link
    Created on : 26.03.2015, 14:49:53
    Author     : Madi
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:if test="${user == null}">
    <script>
        document.location = '<%=request.getContextPath()%>/login';
    </script>
</c:if>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.tag-editor.css" />
<style>

    textarea {
        width: 100%; 
        height: 50px; 
        padding: .3em .5em; 
        border: 1px solid grey; 
        font-size: 12px !important;
        font-family: Verdana !important;
        box-sizing: border-box; 
        margin: 0 0 20px;
    }

    .tag-editor {
        line-height: 30px;
        font-size: 18px;
    }

    #tagarea+.tag-editor { background: #FFF; font-size: 16px; border-radius: 5px; border-color: #ccc}
    #tagarea+.tag-editor .tag-editor-tag {
        color: #fff; background: #555555;
        border-bottom-left-radius: 2px;
    }
    #tagarea+.tag-editor .tag-editor-spacer { width: 7px; }
    #tagarea+.tag-editor .tag-editor-delete { background: #555555;  border-bottom-right-radius: 2px;}

</style>
<script>
    function check() {
        if ($('#categories input:checked').length >= 3) {
            $('#categories input:not(:checked)').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('#categories input:not(:checked)').each(function () {
                $(this).removeAttr('disabled');
            });
        }
    }
</script>
<section class="wrapper style1">
    <div class="container">
        <div id="content">
            <h3>Добавить ссылку</h3>
            <c:if test="${!empty error}">
                <h3 style="color: red">${error}</h3>    
            </c:if>
            <form method="POST" action="<%=request.getContextPath()%>/link-preview">
                <div class="row 100%">
                    <div class="8u 12u(mobilep)">
                        <input type="text" name="url" id="url" placeholder="Ссылка" value="${url}"/>
                    </div>
                    <div class="4u 12u(mobilep)">
                        <ul class="actions">
                            <li><input id="submit" type="submit" class="button alt" value="Предварительный просмотр" /></li>
                        </ul>
                    </div>
                </div>
            </form>
            <br/>
            <c:if test="${!empty post}">
                <div class="row">
                    <section class="9u 12u(narrower)">
                        <div class="box post" style="border: 1px dotted grey; padding: 10px 0 10px 10px !important; margin: 0px !important; border-radius: 5px;">
                            <span class="image left"><img src="${post.photoUrl}" style="max-height: 100px;" alt="" /></span>
                            <div class="inner">
                                <h4 style="margin-bottom: 4px; line-height: 140%"><a href="${post.link}" target="_blank">${post.title}</a> [<a href="${post.homepage}" target="_blank">${post.homepage}</a>]</h4>
                                ${post.description}
                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <section class="12u 12u(narrower)">
                        <form method="POST" action="<%=request.getContextPath()%>/save-link">
                            <div class="row 100%" style="margin-bottom: 20px;">
                                <div class="8u 12u(mobilep)">
                                    <textarea id="tagarea" name="tags"></textarea>
                                </div>   
                            </div>
                            <c:if test="${!empty categories}">
                                <h3>Выберите категорию (макс. 3)</h3>
                                <div class="row 100%" id="categories">
                                    <c:forEach items="${categories}" var="category">
                                        <div class="2u 4u(narrower) 6u(mobilep)">
                                            <input type="checkbox" value="${category.code}" onchange="check()" name="category"/>${category.name}
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                            <div class="row 100%">
                                <input type="hidden" name="url" value="${post.link}"/>
                                <div class="12u 12u(mobilep)">
                                    <ul class="actions">
                                        <li><input id="submit" type="submit" class="button alt" value="Добавить" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </c:if>        
    </div>
</div>
</section>
<script>

    $(function () {

        $.ajax({
            url: '<%=request.getContextPath()%>/tags',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#tagarea').tagEditor({
                    autocomplete: {
                        delay: 0,
                        position: {collision: 'flip'},
                        source: data,
                        minLength: 2},
                    forceLowercase: false,
                    placeholder: 'Теги',
                    maxTags: 10,
                    maxLength: 20
                });
            },
            error: function (e) {

            }
        });

    });

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-63279983-1', 'auto');
    ga('send', 'pageview');

</script>


