<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>buzz.kz</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <meta name="description" content="">
        <c:if test="${!empty post}">
            <meta property="og:title" content="${post.homepage} - ${fn:replace(post.title, "\"", "")}">
            <meta property="og:description" content="${fn:replace(post.description, "\"", "")}">
        </c:if>
        <meta name="robots" content="index, follow">
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.png" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.dropotron.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/skel.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/skel-layers.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/init.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.tag-editor.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.caret.min.js"></script>
        <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css" />
        <noscript>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/skel.css" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/style.css" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/style-wide.css" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.tag-editor.css" />
        </noscript>
        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <style>
            body {
                font-size: 12px !important;
                font-family: Verdana !important;
            }

            a {
                border-bottom: none !important;
            }
        </style>
        <script>
            function like(postId) {
                $.ajax({
                    url: '<%=request.getContextPath()%>/like?postId=' + postId,
                    method: 'POST',
                    success: function (data) {
                        if (data === 'NEW') {
                            var likes = parseInt($('#likes' + postId).html());
                            $('#likes' + postId).html(likes + 1);
                        } else if (data === 'CHANGED') {
                            var likes = parseInt($('#likes' + postId).html());
                            $('#likes' + postId).html(likes + 1);
                            var dislikes = parseInt($('#dislikes' + postId).html());
                            $('#dislikes' + postId).html(dislikes - 1);
                        } else if (data === 'AUTHORIZE') {
                            $("#dialog").dialog({modal: true, width: 240, height: 100});
                        }
                    },
                    error: function (e) {

                    }
                });
            }

            function dislike(postId) {
                $.ajax({
                    url: '<%=request.getContextPath()%>/dislike?postId=' + postId,
                    method: 'POST',
                    success: function (data) {
                        if (data === 'NEW') {
                            var dislikes = parseInt($('#dislikes' + postId).html());
                            $('#dislikes' + postId).html(dislikes + 1);
                        } else if (data === 'CHANGED') {
                            var dislikes = parseInt($('#dislikes' + postId).html());
                            $('#dislikes' + postId).html(dislikes + 1);
                            var likes = parseInt($('#likes' + postId).html());
                            $('#likes' + postId).html(likes - 1);
                        } else if (data === 'AUTHORIZE') {
                            $("#dialog").dialog({modal: true, width: 240, height: 100});
                        }
                    },
                    error: function () {

                    }
                });
            }
        </script>
    </head>
    <body>
        <tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="body" />
        <!--tiles:insertAttribute name="footer" /-->
    </body>
    <div id="dialog" title="Сообщение" style="display: none;">
        <p><a href="<%=request.getContextPath()%>/login" style="text-decoration: underline">Авторизуйтесь</a>, пожалуйста.</p>
    </div>
</html>
